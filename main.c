#include<stm32f30x_i2c.c>


void main(){
    I2C_InitTypeDef a;
    I2C_TypeDef b;
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2Cx, ENABLE); //I2Cx x zmenit na 1 alebo 2, Enable peripheral clock
    RCC_AHBPeriphClockCmd(); //Enable SDA, SCL  and SMBA
    
    
    I2C_StructInit(&a);    // (I2C_InitTypeDef* I2C_InitStruct)
    I2C_Init(&b, &a); //I2C_Init(I2C_TypeDef* I2Cx, I2C_InitTypeDef* I2C_InitStruct) inicializacia i2c komunikacie, funkcia t stm32f30x_i2c
        
    
    
    
    /*##### Communications handling functions #####*/
    I2C_GenerateSTART(I2C_TypeDef* I2Cx, FunctionalState NewState)
    
    
    I2C_GenerateSTOP(I2C_TypeDef* I2Cx, FunctionalState NewState)
    
    
    /*DATA TRANSFER*/
    I2C_SendData(I2C_TypeDef* I2Cx, uint8_t Data); 
    I2C_ReceiveData(I2C_TypeDef* I2Cx); // navratova hdonota uint8_t, 
    
    
    
    
    /*SMBUS management functions
    
     
     
     */
    
    
    
    /*citanie registra  ##### I2C registers management functions #####*/
            /**
        * @brief  Reads the specified I2C register and returns its value.
        * @param  I2Cx: where x can be 1 or 2 to select the I2C peripheral.
        * @param  I2C_Register: specifies the register to read.
        *   This parameter can be one of the following values:
        *     @arg I2C_Register_CR1: CR1 register.
        *     @arg I2C_Register_CR2: CR2 register.
        *     @arg I2C_Register_OAR1: OAR1 register.
        *     @arg I2C_Register_OAR2: OAR2 register.
        *     @arg I2C_Register_TIMINGR: TIMING register.
        *     @arg I2C_Register_TIMEOUTR: TIMEOUTR register.
        *     @arg I2C_Register_ISR: ISR register.
        *     @arg I2C_Register_ICR: ICR register.
        *     @arg I2C_Register_PECR: PECR register.
        *     @arg I2C_Register_RXDR: RXDR register.
        *     @arg I2C_Register_TXDR: TXDR register.
        * @retval The value of the read register.
        */
        uint32_t I2C_ReadRegister(I2C_TypeDef* I2Cx, uint8_t I2C_Register)  



    /* ##### Data transfers management functions #####*/

    I2C_SendData(I2C_TypeDef* I2Cx, uint8_t Data);
    I2C_ReceiveData(I2C_TypeDef* I2Cx); // Returns the most recent received data by the I2Cx peripheral.
    
    I2C_DeInit(I2C_TypeDef* I2Cx);  //Deinitializes the I2Cx peripheral registers to their default reset values.
    
    
}